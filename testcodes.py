import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.feature_selection import RFE
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import confusion_matrix
from sklearn.svm import SVC
from sknn.mlp import Classifier, Layer
from sklearn.pipeline import Pipeline


def read_data(test = False):

    # Reads the data from file, with different column headings for training
    # and test data. Note: file names hardwired and expected to be in working
    # directory
    
    if (test):
        names = ['Weekday', 'Hour', 'Timestamp', 'Log Type', 'User ID', 'User-Agent', 'IP', 'Region', 'City', 'Ad Exchange', 'Domain', 'URL','Anonymous URL ID', 'Ad slot ID', 'Ad slot width', 'Ad slot height', 'Ad slot visibility', 'Ad slot format', 'Ad slot floor price', 'Creative ID', 'Key Page URL', 'Advertiser ID', 'User Tags']
        filename = 'test.txt'
    else:
        names = ['Click', 'Weekday', 'Hour', 'Timestamp', 'Log Type', 'User ID', 'User-Agent', 'IP', 'Region', 'City', 'Ad Exchange', 'Domain', 'URL','Anonymous URL ID', 'Ad slot ID', 'Ad slot width', 'Ad slot height', 'Ad slot visibility', 'Ad slot format', 'Ad slot floor price', 'Creative ID', 'Key Page URL', 'Advertiser ID', 'User Tags']
        filename = 'train.txt'
        
    dataframe = pd.read_csv(filename,sep='\t',names=names)
    return dataframe

def print_n_unique(dataframe):

    # Prints the number of unique values in each column of the dataframe
    
    for i in dataframe.columns.values:
        print i,len(pd.unique(dataframe[i]))

def get_user_tags(dataframe):
    # Splits the user tag string into an array and converts to a dataframe
    # with column headings equal to the various User Tags identified and a
    # 0 or 1 in each column to indicate whether that tag was present in the
    # corresponding row of the input dataframe
    usertags = dataframe['User Tags'].str.get_dummies(sep=',')
    usertags.drop('null',inplace=True,axis=1)
    usertags['ntags'] = usertags.sum(axis=1)
    return usertags

def concat_usertags(dataframe,usertags):

    # Concatenates the usertags and dataframe into one master dataframe
    # containing all the information
    out = pd.concat([dataframe,usertags],axis=1)
    return out

def col_to_numeric(dataframe,colname):
    # Converts a column of string data into a numeric one by assigning a
    # distinct integer value to each unique string found
    agentnames = pd.unique(dataframe[colname])
    mapping = {}
    for i in range(len(agentnames)):
        mapping[agentnames[i]]=i
    return dataframe.applymap(lambda s:mapping.get(s) if s in mapping else s)
    
def prepare_data(dataframe):
    # Runs several scripts above to extract all the potentially relevant
    # features in the data, converting to numerical data where necessary,
    # and returns one large dataframe with all the information. Also creates
    # a column for the total area of each ad
    usertags = get_user_tags(dataframe)
    dataframe = concat_usertags(dataframe,usertags)
    dataframe = col_to_numeric(dataframe,'Creative ID')
    dataframe = col_to_numeric(dataframe,'User-Agent')
    dataframe = col_to_numeric(dataframe,'Key Page URL')
    dataframe['Ad area'] = dataframe['Ad slot width']*dataframe['Ad slot height']
    return dataframe


def random_split_data(dataframe,seed=None):
    # Randomly split the data into 3 roughly equal sections
    # One set is used to train the data, one to test, and one to cross-
    # validate the model

    pd.np.random.seed(seed)
    rands = pd.np.random.rand(len(dataframe))
    mask1 = rands<=0.33
    mask2 = (rands>0.33) & (rands<=0.67)
    mask3 = rands>0.67

    train_data = dataframe[mask1]
    test_data = dataframe[mask2]
    cv_data = dataframe[mask3]

    return train_data,test_data,cv_data

    
def getxy(inp,features,yname = 'Click'):

    # Extracts relevant feature and data columns from the pandas dataframe
    # and formats as arrays for use with sklearn
    X = pd.np.array(inp[features])
    if yname == 'none':
        y = 0
    else:
        y = pd.np.array(inp[yname])
    return X,y

def run_SGD_classifier(train,featurelist, class_weight='balanced'):

    # Runs the stochastic gradient descent classifier
    # Returns the model and the standard feature scaler, to facilitate
    # rescaling of other data sets
    xtrain, ytrain = getxy(train,features = featurelist)
    scaler = StandardScaler()
    scaler.fit(xtrain)
    xtrain = scaler.transform(xtrain)
    clf = SGDClassifier(class_weight=class_weight)
    clf.fit(xtrain,ytrain)
    print 'Training data'
    print clf.score(xtrain,ytrain)
    print confusion_matrix(ytrain, clf.predict(xtrain))

    return clf,scaler

def undersample(x,nsamples):
    # Returns a dataframe with all of the positive click data and nsamples
    # of randomly-sampled negative click data
    
    zero_indices = x[x['Click']==0].index
    random_indices = pd.np.random.choice(zero_indices,nsamples,replace=False)
    zero_sample = x.loc[random_indices]
    resampled = pd.concat([x[x['Click']==1],zero_sample])
    return resampled
    
def run_nn(xtrain,ytrain):
    # Runs a simple neural network with 1 hidden layer
    # Note: takes ~30 min to run on 1/3 of the training data
    pipeline = Pipeline([
        ('min/max scaler', StandardScaler()),
        ('neural network', Classifier(layers=[Layer("Sigmoid",units=100),Layer("Softmax")], n_iter=25))])
    pipeline.fit(xtrain, ytrain)
    pipeline.score(xtrain,ytrain)
    print sum(pipeline.predict(xtrain)) # This gives the total number of clicks predicted to be 1
    print confusion_matrix(ytrain,pipeline.predict(xtrain))
    return pipeline


def run_SVC(xtrain,ytrain,featurelist):

    # Runs a support vector classifier. Should only be used on smaller
    # training sets due to runtime
    
    clf = SVC(class_weight='balanced')
    scaler = StandardScaler()
    scaler.fit(xtrain)
    xtrain = scaler.transform(xtrain)
    clf.fit(xtrain,ytrain)
    print clf.score(xtrain,ytrain)

    return clf,scaler

if __name__=='__main__':

    # Read in the training data and the test data
    print "Reading in data"
    training_data = read_data()
    test_data = read_data(test = True)

    # Prepare the features
    print "Preparing features"
    training_data = prepare_data(training_data)
    test_data = prepare_data(test_data)

    # Set the features to be used

    featurelist = list(['Weekday','Hour','Region', 'City','ntags', 'Ad slot width','Ad slot height', 'Ad slot visibility', 'Ad slot format','Ad slot floor price','10006', '10024', '10031', '10048', '10052', '10057', '10059','10063', '10067', '10074', '10075', '10076', '10077', '10079','10083', '10093', '10102', '10110', '10111', '10684', '11092','11278', '11379', '11423', '11512', '11576', '11632', '11680','11724', '11944', '13042', '13403', '13496', '13678', '13776','13800', '13866', '13874', '14273', '16593', '16617', '16661','16706','User-Agent','Ad area','Creative ID','Key Page URL'])
    
    print "Training model"    
    model,scaler = run_SGD_classifier(training_data,featurelist)

    # Reformat test data into X array

    xtest,dum = getxy(test_data,features=featurelist,yname='none')

    # Rescale the data in the same way that the training data were scaled

    xtest = scaler.transform(xtest)

    # Use the model to predict the click values
    
    print "Making predictions"
    ypred = model.predict(xtest)

    print "Writing to file"
    pd.np.savetxt('predictions.txt',ypred)
    

    
